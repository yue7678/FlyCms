package com.flycms;


import com.flycms.modules.elastic.service.impl.ElasticSearchServiceImpl;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.data.service.IIpAddressService;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringDataElasticsearchFindTest {


    @Autowired
    private ElasticSearchServiceImpl elasticSearchServiceImpl;


    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IIpAddressService ipAddressService;

   @Test
    public void indexAllTopic() {

            List<GroupTopicDTO> topics = groupTopicService.selectGroupTopicAllList();
            topics.stream().collect(Collectors.toMap(key -> String.valueOf(key
                    .getId()), value -> {
                GroupTopic groupTopic =new GroupTopic();
                groupTopic.setTitle("梦见"+value.getTitle()+"是什么意思");
                groupTopic.setId(value.getId());
                return groupTopicService.updateGroupTopic(groupTopic);
            }));

    }

    /**
     * 查询全部
     */
/*    @Test
    // 创建索引
    public void createIndexStu() {
        DreamDocument document = new DreamDocument();
        document.setId(1l);
        document.setTitle("标题");
        document.setContent("内容");
        dreamDocumentRepository.save(document);
    }*/

/*    @Test
    public void setEmail() throws Exception {
        List<IpAddressDTO> list= ipAddressService.exportIpAddressList(null);
        list.stream().collect(Collectors.toMap(key -> String.valueOf(key
                .getId()), value -> {
            IpAddress ipAddress =new IpAddress();
            ipAddress.setStartIp(String.valueOf(IpUtils.ip2Long(value.getStartIp())));
            ipAddress.setEndIp(String.valueOf(IpUtils.ip2Long(value.getEndIp())));
            ipAddress.setId(value.getId());
            return ipAddressService.updateIpAddress(ipAddress);

        }));
    }*/

    //获取中文的首字母
    @Test
    public void testPinyin() throws BadHanyuPinyinOutputFormatCombination {
        String name = "x互xx";
        char[] charArray = name.toCharArray();
        StringBuilder pinyin = new StringBuilder();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        // 设置大小写格式
        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        // 设置声调格式：
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < charArray.length; i++) {
            //匹配中文,非中文转换会转换成null
            if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                String[] hanyuPinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                if (hanyuPinyinStringArray != null) {
                    pinyin.append(hanyuPinyinStringArray[0].charAt(0));
                }
            }
        }
        System.err.println(pinyin);
    }

}
