package com.flycms.modules.notify.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.EmailTemplet;
import org.springframework.stereotype.Repository;

/**
 * 系统邮件模板设置Mapper接口
 * 
 * @author admin
 * @date 2020-11-09
 */
@Repository
public interface EmailTempletMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增系统邮件模板设置
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 结果
     */
    public int insertEmailTemplet(EmailTemplet emailTemplet);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除系统邮件模板设置
     *
     * @param id 系统邮件模板设置ID
     * @return 结果
     */
    public int deleteEmailTempletById(Long id);

    /**
     * 批量删除系统邮件模板设置
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteEmailTempletByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改系统邮件模板设置
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 结果
     */
    public int updateEmailTemplet(EmailTemplet emailTemplet);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询系统邮件模板设置
     * 
     * @param id 系统邮件模板设置ID
     * @return 系统邮件模板设置
     */
    public EmailTemplet findEmailTempletById(Long id);

    /**
     * 按模板标记码查询配置信息
     *
     * @param tpCode
     * @return
     */
    public EmailTemplet findEmailTempletByTpCode(String tpCode);

    /**
     * 查询系统邮件模板设置数量
     *
     * @param pager 分页处理类
     * @return 系统邮件模板设置数量
     */
    public int queryEmailTempletTotal(Pager pager);

    /**
     * 查询系统邮件模板设置列表
     * 
     * @param pager 分页处理类
     * @return 系统邮件模板设置集合
     */
    public List<EmailTemplet> selectEmailTempletPager(Pager pager);

    /**
     * 查询需要导出的系统邮件模板设置列表
     *
     * @param emailTemplet 系统邮件模板设置
     * @return 系统邮件模板设置集合
     */
    public List<EmailTemplet> exportEmailTempletList(EmailTemplet emailTemplet);
}
