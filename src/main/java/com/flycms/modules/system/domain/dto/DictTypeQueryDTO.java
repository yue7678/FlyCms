package com.flycms.modules.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 字典类型数据传输对象 fly_dict_type
 *
 * @author admin
 * @date 2020-07-05
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class DictTypeQueryDTO
{
    private static final long serialVersionUID = 1L;

    /** 字典主键 */
    @Excel(name = "字典主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dictId;
    /** 字典名称 */
    @Excel(name = "字典名称")
    private String dictName;
    /** 字典类型 */
    @Excel(name = "字典类型")
    private String dictType;
    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}