package com.flycms.modules.statistics.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.statistics.domain.StatisticsAccessRecord;
import com.flycms.modules.statistics.domain.dto.StatisticsAccessRecordDTO;

import java.util.List;

/**
 * 访问记录Service接口
 * 
 * @author admin
 * @date 2020-12-09
 */
public interface IStatisticsAccessRecordService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增访问记录
     *
     * @param statisticsAccessRecord 访问记录
     * @return 结果
     */
    public int insertStatisticsAccessRecord(StatisticsAccessRecord statisticsAccessRecord);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除访问记录
     *
     * @param ids 需要删除的访问记录ID
     * @return 结果
     */
    public int deleteStatisticsAccessRecordByIds(Long[] ids);

    /**
     * 删除访问记录信息
     *
     * @param id 访问记录ID
     * @return 结果
     */
    public int deleteStatisticsAccessRecordById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改访问记录
     *
     * @param statisticsAccessRecord 访问记录
     * @return 结果
     */
    public int updateStatisticsAccessRecord(StatisticsAccessRecord statisticsAccessRecord);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询访问记录
     * 
     * @param id 访问记录ID
     * @return 访问记录
     */
    public StatisticsAccessRecordDTO findStatisticsAccessRecordById(Long id);

    /**
     * 查询访问记录列表
     * 
     * @param statisticsAccessRecord 访问记录
     * @return 访问记录集合
     */
    public Pager<StatisticsAccessRecordDTO> selectStatisticsAccessRecordPager(StatisticsAccessRecord statisticsAccessRecord, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的访问记录列表
     *
     * @param statisticsAccessRecord 访问记录
     * @return 访问记录集合
     */
    public List<StatisticsAccessRecordDTO> exportStatisticsAccessRecordList(StatisticsAccessRecord statisticsAccessRecord);
}
