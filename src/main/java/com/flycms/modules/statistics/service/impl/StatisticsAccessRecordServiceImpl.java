package com.flycms.modules.statistics.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.statistics.mapper.StatisticsAccessRecordMapper;
import com.flycms.modules.statistics.domain.StatisticsAccessRecord;
import com.flycms.modules.statistics.domain.dto.StatisticsAccessRecordDTO;
import com.flycms.modules.statistics.service.IStatisticsAccessRecordService;

import java.util.ArrayList;
import java.util.List;

/**
 * 访问记录Service业务层处理
 * 
 * @author admin
 * @date 2020-12-09
 */
@Service
public class StatisticsAccessRecordServiceImpl implements IStatisticsAccessRecordService 
{
    @Autowired
    private StatisticsAccessRecordMapper statisticsAccessRecordMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增访问记录
     *
     * @param statisticsAccessRecord 访问记录
     * @return 结果
     */
    @Override
    public int insertStatisticsAccessRecord(StatisticsAccessRecord statisticsAccessRecord)
    {
        statisticsAccessRecord.setId(SnowFlakeUtils.nextId());
        statisticsAccessRecord.setCreateTime(DateUtils.getNowDate());
        return statisticsAccessRecordMapper.insertStatisticsAccessRecord(statisticsAccessRecord);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除访问记录
     *
     * @param ids 需要删除的访问记录ID
     * @return 结果
     */
    @Override
    public int deleteStatisticsAccessRecordByIds(Long[] ids)
    {
        return statisticsAccessRecordMapper.deleteStatisticsAccessRecordByIds(ids);
    }

    /**
     * 删除访问记录信息
     *
     * @param id 访问记录ID
     * @return 结果
     */
    @Override
    public int deleteStatisticsAccessRecordById(Long id)
    {
        return statisticsAccessRecordMapper.deleteStatisticsAccessRecordById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改访问记录
     *
     * @param statisticsAccessRecord 访问记录
     * @return 结果
     */
    @Override
    public int updateStatisticsAccessRecord(StatisticsAccessRecord statisticsAccessRecord)
    {
        statisticsAccessRecord.setUpdateTime(DateUtils.getNowDate());
        return statisticsAccessRecordMapper.updateStatisticsAccessRecord(statisticsAccessRecord);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询访问记录
     * 
     * @param id 访问记录ID
     * @return 访问记录
     */
    @Override
    public StatisticsAccessRecordDTO findStatisticsAccessRecordById(Long id)
    {
        StatisticsAccessRecord statisticsAccessRecord=statisticsAccessRecordMapper.findStatisticsAccessRecordById(id);
        return BeanConvertor.convertBean(statisticsAccessRecord,StatisticsAccessRecordDTO.class);
    }


    /**
     * 查询访问记录列表
     *
     * @param statisticsAccessRecord 访问记录
     * @return 访问记录
     */
    @Override
    public Pager<StatisticsAccessRecordDTO> selectStatisticsAccessRecordPager(StatisticsAccessRecord statisticsAccessRecord, Integer page, Integer limit, String sort, String order)
    {
        Pager<StatisticsAccessRecordDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(statisticsAccessRecord);

        List<StatisticsAccessRecord> statisticsAccessRecordList=statisticsAccessRecordMapper.selectStatisticsAccessRecordPager(pager);
        List<StatisticsAccessRecordDTO> dtolsit = new ArrayList<StatisticsAccessRecordDTO>();
        statisticsAccessRecordList.forEach(entity -> {
            StatisticsAccessRecordDTO dto = new StatisticsAccessRecordDTO();
            dto.setId(entity.getId());
            dto.setIsLogin(entity.getIsLogin());
            dto.setLoginUserId(entity.getLoginUserId());
            dto.setLoginUserName(entity.getLoginUserName());
            dto.setSessionId(entity.getSessionId());
            dto.setCookieId(entity.getCookieId());
            dto.setAccessSourceClient(entity.getAccessSourceClient());
            dto.setAccessUrl(entity.getAccessUrl());
            dto.setSourceUrl(entity.getSourceUrl());
            dto.setSourceDomain(entity.getSourceDomain());
            dto.setSorceUrlType(entity.getSorceUrlType());
            dto.setAccessIp(entity.getAccessIp());
            dto.setAccessDevice(entity.getAccessDevice());
            dto.setAccessCity(entity.getAccessCity());
            dto.setAccessBrowser(entity.getAccessBrowser());
            dto.setAccessCountry(entity.getAccessCountry());
            dto.setAccessProvince(entity.getAccessProvince());
            dto.setEngineName(entity.getEngineName());
            dto.setIsNewVisitor(entity.getIsNewVisitor());
            dto.setDeviceType(entity.getDeviceType());
            dto.setDeleted(entity.getDeleted());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(statisticsAccessRecordMapper.queryStatisticsAccessRecordTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的访问记录列表
     *
     * @param statisticsAccessRecord 访问记录
     * @return 访问记录集合
     */
    @Override
    public List<StatisticsAccessRecordDTO> exportStatisticsAccessRecordList(StatisticsAccessRecord statisticsAccessRecord) {
        return BeanConvertor.copyList(statisticsAccessRecordMapper.exportStatisticsAccessRecordList(statisticsAccessRecord),StatisticsAccessRecordDTO.class);
    }
}
