package com.flycms.modules.tool.mapper;

import java.util.List;

import com.flycms.modules.tool.domain.GenTable;
import com.flycms.common.utils.page.Pager;
import org.springframework.stereotype.Repository;

/**
 * 业务 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface GenTableMapper
{
    /**
     * 查询表名称是否已存在
     *
     * @param tableName 表名称
     * @return 业务信息
     */
    public int checkGenTableByName(String tableName);

    /**
     * 查询业务数量
     *
     * @param pager 业务信息
     * @return 业务数量
     */
    public int queryGenTableTotal(Pager pager);

    /**
     * 查询业务列表
     * 
     * @param pager 业务信息
     * @return 业务集合
     */
    public List<GenTable> selectGenTableList(Pager pager);


    /**
     * 查询数据库表数量
     *
     * @param pager 店铺
     * @return 订单数量
     */
    public int queryDbTableTotal(Pager pager);

    /**
     * 查询数据库列表
     * 
     * @param pager 业务信息
     * @return 数据库表集合
     */
    public List<GenTable> selectDbTableList(Pager pager);

    /**
     * 查询据库列表
     * 
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public List<GenTable> selectDbTableListByNames(String[] tableNames);

    /**
     * 查询表ID业务信息
     * 
     * @param id 业务ID
     * @return 业务信息
     */
    public GenTable selectGenTableById(Long id);

    /**
     * 查询表名称业务信息
     * 
     * @param tableName 表名称
     * @return 业务信息
     */
    public GenTable selectGenTableByName(String tableName);

    /**
     * 查询表名称业务信息
     *
     * @param tableName 表名称
     * @return 业务信息
     */
    public GenTable selectDBTableByName(String tableName);

    /**
     * 新增业务
     * 
     * @param genTable 业务信息
     * @return 结果
     */
    public int insertGenTable(GenTable genTable);

    /**
     * 修改业务
     * 
     * @param genTable 业务信息
     * @return 结果
     */
    public int updateGenTable(GenTable genTable);

    /**
     * 批量删除业务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGenTableByIds(Long[] ids);
}