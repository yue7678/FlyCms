package com.flycms.modules.user.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserActivation;
import com.flycms.modules.user.domain.dto.UserActivationDTO;

import java.util.List;

/**
 * 注册码Service接口
 * 
 * @author admin
 * @date 2020-12-08
 */
public interface IUserActivationService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增注册码
     *
     * @param userActivation 注册码
     * @return 结果
     */
    public int insertUserActivation(UserActivation userActivation);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除注册码
     *
     * @param ids 需要删除的注册码ID
     * @return 结果
     */
    public int deleteUserActivationByIds(Long[] ids);

    /**
     * 删除注册码信息
     *
     * @param id 注册码ID
     * @return 结果
     */
    public int deleteUserActivationById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改注册码
     *
     * @param userActivation 注册码
     * @return 结果
     */
    public int updateUserActivation(UserActivation userActivation);
    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param userName 用户名
     * @param code 验证码
     * @return 结果
     */
    public int updateUserActivationByStatus(String userName,String code);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userName
     *         查询的用户名
     * @param codeType
     *         查询的验证码类型，1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @param code
     *         验证码
     * @return
     */
    public boolean checkUserActivationCode(String userName,Integer codeType,String code);

    /**
     * 查询注册码
     * 
     * @param id 注册码ID
     * @return 注册码
     */
    public UserActivationDTO findUserActivationById(Long id);

    /**
     * 查询注册码列表
     * 
     * @param userActivation 注册码
     * @return 注册码集合
     */
    public Pager<UserActivationDTO> selectUserActivationPager(UserActivation userActivation, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的注册码列表
     *
     * @param userActivation 注册码
     * @return 注册码集合
     */
    public List<UserActivationDTO> exportUserActivationList(UserActivation userActivation);
}
