package com.flycms.modules.user.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserInfoDTO  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Excel(name = "ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/** 账号 */
	@Excel(name = "账号")
	private String username;
	/** 手机号码 */
	@Excel(name = "手机号码")
	private String mobile;
	/** 邮箱 */
	@Excel(name = "邮箱")
	private String email;
	/** 性别 */
	@Excel(name = "性别")
	private Integer gender;
	/** 生日 */
	@Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
	private Date birthday;
	/** 登录时间 */
	@Excel(name = "登录时间", width = 30, dateFormat = "yyyy-MM-dd")
	private Date lastLoginTime;
	/** 用户昵称或网络名称 */
	@Excel(name = "昵称")
	private String nickname;
	/** 用户头像图片 */
	@Excel(name = "头像")
	private String avatar;
	/** 个性签名 */
	@Excel(name = "个性签名")
	private String signature;
	/** 个人介绍 */
	@Excel(name = "个人介绍")
	private String about;
	/** 用户余额 */
	@Excel(name = "用户余额")
	private Double balance;
	/** 积分 */
	@Excel(name = "积分")
	private Integer score;
	/** 经验值 */
	@Excel(name = "经验值")
	private Integer exp;
	/** 话题数量 */
	@Excel(name = "话题数量")
	private Integer countTopic;
	/** 加入小组数 */
	@Excel(name = "加入小组数")
	private Integer countGroup;
	/** 粉丝数 */
	@Excel(name = "粉丝数")
	private Integer countFans;
	/** 被注数 */
	@Excel(name = "被注数")
	private Integer countFollw;
	/** 0 可用, 1 禁用, 2 注销 */
	@Excel(name = "审核")
	private Integer status;
}
