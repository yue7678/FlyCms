package com.flycms.modules.user.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.user.domain.UserAccount;
import com.flycms.modules.user.domain.dto.UserAccountDTO;
import com.flycms.modules.user.service.IUserAccountService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 用户账户Controller
 * 
 * @author admin
 * @date 2020-12-04
 */
@RestController
@RequestMapping("/system/user/account")
public class UserAccountAdminController extends BaseController
{
    @Autowired
    private IUserAccountService userAccountService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增用户账户
     */
    @PreAuthorize("@ss.hasPermi('user:account:add')")
    @Log(title = "用户账户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserAccount userAccount)
    {
        if (UserConstants.NOT_UNIQUE.equals(userAccountService.checkUserAccountUserIdUnique(userAccount.getUserId())))
        {
            return AjaxResult.error("新增用户账户'" + userAccount.getUserId() + "'失败，用户id已存在");
        }
        return toAjax(userAccountService.insertUserAccount(userAccount));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户账户
     */
    @PreAuthorize("@ss.hasPermi('user:account:remove')")
    @Log(title = "用户账户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(userAccountService.deleteUserAccountByIds(userIds));
    }



    /**
     * 修改用户账户
     */
    @PreAuthorize("@ss.hasPermi('user:account:edit')")
    @Log(title = "用户账户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserAccount userAccount)
    {
        if (UserConstants.NOT_UNIQUE.equals(userAccountService.checkUserAccountUserIdUnique(userAccount.getUserId())))
        {
            return AjaxResult.error("新增用户账户'" + userAccount.getUserId() + "'失败，用户id已存在");
        }
        return toAjax(userAccountService.updateUserAccount(userAccount));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询用户账户列表
     */
    @PreAuthorize("@ss.hasPermi('user:account:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserAccount userAccount,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<UserAccountDTO> pager = userAccountService.selectUserAccountPager(userAccount, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出用户账户列表
     */
    @PreAuthorize("@ss.hasPermi('user:account:export')")
    @Log(title = "用户账户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserAccount userAccount)
    {
        List<UserAccountDTO> userAccountList = userAccountService.exportUserAccountList(userAccount);
        ExcelUtil<UserAccountDTO> util = new ExcelUtil<UserAccountDTO>(UserAccountDTO.class);
        return util.exportExcel(userAccountList, "account");
    }

    /**
     * 获取用户账户详细信息
     */
    @PreAuthorize("@ss.hasPermi('user:account:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(userAccountService.findUserAccountById(userId));
    }

}
