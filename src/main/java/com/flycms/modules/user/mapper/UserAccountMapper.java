package com.flycms.modules.user.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserAccount;
import org.springframework.stereotype.Repository;

/**
 * 用户账户Mapper接口
 * 
 * @author admin
 * @date 2020-12-04
 */
@Repository
public interface UserAccountMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户账户
     *
     * @param userAccount 用户账户
     * @return 结果
     */
    public int insertUserAccount(UserAccount userAccount);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户账户
     *
     * @param userId 用户账户ID
     * @return 结果
     */
    public int deleteUserAccountById(Long userId);

    /**
     * 批量删除用户账户
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserAccountByIds(Long[] userIds);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户账户
     *
     * @param userAccount 用户账户
     * @return 结果
     */
    public int updateUserAccount(UserAccount userAccount);

    /**
     * 更新用户发布话题数量
     *
     * @param userId 用户id
     * @return
     */
    public int updateUserTopic(Long userId);

    /**
     * 更新用户加入的小组数量
     *
     * @param userId 用户id
     * @return
     */
    public int updateUserGroup(Long userId);

    /**
     * 更新用户所有粉丝数量
     *
     * @param userId 用户id
     * @return
     */
    public int updateUserFans(Long userId);

    /**
     * 更新用户被关注数量
     *
     * @param userId 用户id
     * @return
     */
    public int updateUserFollw(Long userId);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验用户id是否唯一
     *
     * @param userAccount 用户账户ID
     * @return 结果
     */
    public int checkUserAccountUserIdUnique(UserAccount  userAccount);


    /**
     * 查询用户账户
     * 
     * @param userId 用户账户ID
     * @return 用户账户
     */
    public UserAccount findUserAccountById(Long userId);

    /**
     * 查询用户账户数量
     *
     * @param pager 分页处理类
     * @return 用户账户数量
     */
    public int queryUserAccountTotal(Pager pager);

    /**
     * 查询用户账户列表
     * 
     * @param pager 分页处理类
     * @return 用户账户集合
     */
    public List<UserAccount> selectUserAccountPager(Pager pager);

    /**
     * 查询需要导出的用户账户列表
     *
     * @param userAccount 用户账户
     * @return 用户账户集合
     */
    public List<UserAccount> exportUserAccountList(UserAccount userAccount);
}
