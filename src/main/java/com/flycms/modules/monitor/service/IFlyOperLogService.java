package com.flycms.modules.monitor.service;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.OperLog;
import com.flycms.modules.monitor.domain.dto.OperLogDTO;

/**
 * 操作日志 服务层
 * 
 * @author kaifei sun
 */
public interface IFlyOperLogService
{
    /**
     * 新增操作日志
     * 
     * @param operLog 操作日志对象
     */
    public void insertOperlog(OperLog operLog);

    /**
     * 批量删除系统操作日志
     * 
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public int deleteOperLogByIds(Long[] operIds);

    /**
     * 查询操作日志详细
     * 
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public OperLog selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();


    /**
     * 查询操作日志记录列表
     *
     * @param operLog 操作日志记录
     * @return 操作日志记录
     */
    public Pager<OperLogDTO> selectOperLogPager(OperLog operLog, Integer page, Integer limit, String sort, String order);


    /**
     * 查询需要导出的操作日志记录列表
     *
     * @param operLog 操作日志记录
     * @return 操作日志记录集合
     */
    public List<OperLogDTO> exportOperLogList(OperLog operLog);
}
