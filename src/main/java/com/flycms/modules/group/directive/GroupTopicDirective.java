package com.flycms.modules.group.directive;

import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.GroupTopicColumn;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupTopicColumnService;
import com.flycms.modules.group.service.IGroupTopicService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GroupTopicDirective extends BaseTag {

    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;

    public GroupTopicDirective() {
        super(GroupTopicDirective.class.getName());
    }

    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String title = getParam(params, "title");
        Long columnId = getLongParam(params, "column");
        Long groupId = getLongParam(params, "groupId");
        Long userId = getLongParam(params, "userId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        int isaudit = getStatusParam(params,"isaudit");
        int deleted = getDeletedTopic(params);
        GroupTopic groupTopic = new GroupTopic();
        groupTopic.setGroupId(groupId);
        groupTopic.setColumnId(columnId);
        groupTopic.setUserId(userId);
        groupTopic.setStatus(status);
        groupTopic.setTitle(title);
        groupTopic.setIsaudit(isaudit);
        groupTopic.setDeleted(deleted);
        return groupTopicService.selectGroupTopicPager(groupTopic, pageNum, pageSize, sort, order);
    }

    public Object column(Map params) {
        String title = getParam(params, "title");
        Long columnId = getLongParam(params, "columnId");
        Long groupId = getLongParam(params, "groupId");
        int status = getStatusParam(params,"status");
        GroupTopicColumn groupTopicColumn = new GroupTopicColumn();
        if(!StrUtils.isEmpty(title) && columnId == null && groupId==null){
            return null;
        }
        groupTopicColumn.setGroupId(groupId);
        groupTopicColumn.setColumnName(title);
        groupTopicColumn.setStatus(status);
        return groupTopicColumnService.selectGroupTopicColumnList(groupTopicColumn);
    }

    /**
     * 小组详细信息
     *
     * @param params
     * @return
     */
    public Object info(Map params) {
        Long id = getLongParam(params, "id");
        GroupTopicDTO topic = null;
        if(id != null){
            topic = groupTopicService.findGroupTopicById(id);
        }
        return topic;
    }
}
