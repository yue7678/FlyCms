package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicComment;
import org.springframework.stereotype.Repository;

/**
 * 话题回复/评论Mapper接口
 * 
 * @author admin
 * @date 2020-12-15
 */
@Repository
public interface GroupTopicCommentMapper
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int insertFlyGroupTopicComment(GroupTopicComment groupTopicComment);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题回复/评论
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    public int deleteFlyGroupTopicCommentById(Long id);

    /**
     * 批量删除话题回复/评论
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFlyGroupTopicCommentByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int updateFlyGroupTopicComment(GroupTopicComment groupTopicComment);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询话题回复/评论
     * 
     * @param id 话题回复/评论ID
     * @return 话题回复/评论
     */
    public GroupTopicComment findFlyGroupTopicCommentById(Long id);

    /**
     * 查询话题回复/评论数量
     *
     * @param pager 分页处理类
     * @return 话题回复/评论数量
     */
    public int queryFlyGroupTopicCommentTotal(Pager pager);

    /**
     * 查询话题回复/评论列表
     * 
     * @param pager 分页处理类
     * @return 话题回复/评论集合
     */
    public List<GroupTopicComment> selectFlyGroupTopicCommentPager(Pager pager);

    /**
     * 查询需要导出的话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    public List<GroupTopicComment> exportFlyGroupTopicCommentList(GroupTopicComment groupTopicComment);
}
