package com.flycms.modules.group.domain.vo;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 小组话题对象 fly_group_topic
 * 
 * @author admin
 * @date 2020-09-27
 */
@Data
public class GroupTopicVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 话题ID */
    private String id;
    /** 分类ID */
    private String columnId;
    /** 小组ID */
    private String groupId;
    /** 用户ID */
    private String userId;
    /** 帖子标题 */
    private String title;
    /** 快速标注 */
    private String label;
    /** 帖子内容 */
    private String content;
    /** 是否关闭 */
    private Integer isclose;
    /** 是否允许评论 */
    private Integer iscomment;
    /** 是否评论后显示内容 */
    private Integer iscommentshow;
    /** 查看所需积分 */
    private Integer score;
}
