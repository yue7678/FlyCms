package com.flycms.modules.group.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 内容推荐对象 fly_group_topic_recommend
 * 
 * @author admin
 * @date 2020-12-07
 */
@Data
public class GroupTopicRecommend extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 小组ID */
    private Long groupId;
    /** 内容类型id */
    private Long contentTypeId;
    /** 内容id */
    private Long contentId;
    /** 推荐设置 */
    private Integer recommend;
    /** 设置管理员 */
    private Long adminId;
}
