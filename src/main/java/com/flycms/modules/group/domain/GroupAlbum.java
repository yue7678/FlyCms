package com.flycms.modules.group.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 小组帖子专辑对象 fly_group_album
 * 
 * @author admin
 * @date 2020-12-16
 */
@Data
public class GroupAlbum extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 专辑ID */
    private Long id;
    /** 用户ID */
    private Long userId;
    /** 小组ID */
    private Long groupId;
    /** 专辑封面 */
    private String albumPic;
    /** 专辑名字 */
    private String albumName;
    /** 专辑介绍 */
    private String albumDesc;
    /** 统计帖子 */
    private Long countTopic;
    /** 审核 */
    private Integer isaudit;
}
