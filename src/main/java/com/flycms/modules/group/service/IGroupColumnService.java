package com.flycms.modules.group.service;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupColumn;
import com.flycms.modules.group.domain.dto.GroupColumnDTO;

/**
 * 小组分类Service接口
 * 
 * @author admin
 * @date 2020-09-25
 */
public interface IGroupColumnService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组分类
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    public int insertGroupColumn(GroupColumn groupColumn);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组分类
     *
     * @param ids 需要删除的小组分类ID
     * @return 结果
     */
    public int deleteGroupColumnByIds(Long[] ids);

    /**
     * 删除小组分类信息
     *
     * @param id 小组分类ID
     * @return 结果
     */
    public int deleteGroupColumnById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组分类
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    public int updateGroupColumn(GroupColumn groupColumn);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验分类名称是否唯一
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    public String checkGroupColumnColumnNameUnique(GroupColumn groupColumn);


    /**
     * 查询小组分类
     * 
     * @param id 小组分类ID
     * @return 小组分类
     */
    public GroupColumnDTO findGroupColumnById(Long id);

    /**
     * 查询小组分类所有列表
     *
     * @param groupColumn 小组分类
     * @return 小组分类集合
     */
    public List<GroupColumnDTO> selectGroupColumnList(GroupColumn groupColumn);
}
