package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import com.flycms.modules.group.domain.dto.GroupUserIsauditDTO;

import java.util.List;

/**
 * 小组成员审核Service接口
 * 
 * @author admin
 * @date 2020-11-24
 */
public interface IGroupUserIsauditService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组成员审核
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 结果
     */
    public int insertGroupUserIsaudit(GroupUserIsaudit groupUserIsaudit);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组成员审核
     *
     * @param userIds 需要删除的小组成员审核ID
     * @return 结果
     */
    public int deleteGroupUserIsauditByIds(Long[] userIds);

    /**
     * 删除小组成员审核信息
     *
     * @param userId 小组成员审核ID
     * @return 结果
     */
    public int deleteGroupUserIsauditById(Long userId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组成员审核
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 结果
     */
    public int updateGroupUserIsaudit(GroupUserIsaudit groupUserIsaudit);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询小组成员审核
     * 
     * @param userId 小组成员审核ID
     * @return 小组成员审核
     */
    public GroupUserIsaudit findGroupUserIsauditById(Long userId,Long groupId);

    /**
     * 查询小组成员审核列表
     * 
     * @param groupUserIsaudit 小组成员审核
     * @return 小组成员审核集合
     */
    public Pager<GroupUserIsauditDTO> selectGroupUserIsauditPager(GroupUserIsaudit groupUserIsaudit, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的小组成员审核列表
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 小组成员审核集合
     */
    public List<GroupUserIsauditDTO> exportGroupUserIsauditList(GroupUserIsaudit groupUserIsaudit);
}
