package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupTopicCommentMapper;
import com.flycms.modules.group.domain.GroupTopicComment;
import com.flycms.modules.group.domain.dto.GroupTopicCommentDTO;
import com.flycms.modules.group.service.IGroupTopicCommentService;

import java.util.ArrayList;
import java.util.List;

/**
 * 话题回复/评论Service业务层处理
 * 
 * @author admin
 * @date 2020-12-15
 */
@Service
public class GroupTopicCommentServiceImpl implements IGroupTopicCommentService
{
    @Autowired
    private GroupTopicCommentMapper groupTopicCommentMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    @Override
    public int insertFlyGroupTopicComment(GroupTopicComment groupTopicComment)
    {
        groupTopicComment.setId(SnowFlakeUtils.nextId());
        groupTopicComment.setUserId(SessionUtils.getUser().getId());
        groupTopicComment.setCreateTime(DateUtils.getNowDate());
        return groupTopicCommentMapper.insertFlyGroupTopicComment(groupTopicComment);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除话题回复/评论
     *
     * @param ids 需要删除的话题回复/评论ID
     * @return 结果
     */
    @Override
    public int deleteFlyGroupTopicCommentByIds(Long[] ids)
    {
        return groupTopicCommentMapper.deleteFlyGroupTopicCommentByIds(ids);
    }

    /**
     * 删除话题回复/评论信息
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    @Override
    public int deleteFlyGroupTopicCommentById(Long id)
    {
        return groupTopicCommentMapper.deleteFlyGroupTopicCommentById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    @Override
    public int updateFlyGroupTopicComment(GroupTopicComment groupTopicComment)
    {
        return groupTopicCommentMapper.updateFlyGroupTopicComment(groupTopicComment);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询话题回复/评论
     * 
     * @param id 话题回复/评论ID
     * @return 话题回复/评论
     */
    @Override
    public GroupTopicCommentDTO findFlyGroupTopicCommentById(Long id)
    {
        GroupTopicComment groupTopicComment = groupTopicCommentMapper.findFlyGroupTopicCommentById(id);
        return BeanConvertor.convertBean(groupTopicComment, GroupTopicCommentDTO.class);
    }


    /**
     * 查询话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论
     */
    @Override
    public Pager<GroupTopicCommentDTO> selectFlyGroupTopicCommentPager(GroupTopicComment groupTopicComment, Integer page, Integer limit, String sort, String order)
    {
        Pager<GroupTopicCommentDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopicComment);

        List<GroupTopicComment> groupTopicCommentList = groupTopicCommentMapper.selectFlyGroupTopicCommentPager(pager);
        List<GroupTopicCommentDTO> dtolsit = new ArrayList<GroupTopicCommentDTO>();
        groupTopicCommentList.forEach(entity -> {
            GroupTopicCommentDTO dto = new GroupTopicCommentDTO();
            dto.setId(entity.getId());
            dto.setReferId(entity.getReferId());
            dto.setTopicId(entity.getTopicId());
            dto.setUserId(entity.getUserId());
            dto.setContent(entity.getContent());
            dto.setIspublic(entity.getIspublic());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicCommentMapper.queryFlyGroupTopicCommentTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    @Override
    public List<GroupTopicCommentDTO> exportFlyGroupTopicCommentList(GroupTopicComment groupTopicComment) {
        return BeanConvertor.copyList(groupTopicCommentMapper.exportFlyGroupTopicCommentList(groupTopicComment), GroupTopicCommentDTO.class);
    }
}
