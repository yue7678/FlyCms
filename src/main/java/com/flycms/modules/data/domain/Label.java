package com.flycms.modules.data.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 标签对象 fly_label
 * 
 * @author admin
 * @date 2020-11-18
 */
@Data
public class Label extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 标签名称 */
    private String title;
    /** 标签说明 */
    private String content;
    /** 使用数量 */
    private Integer countTopic;
    /** 使用数量 */
    private Integer countGroup;
    /** 创建时间 */
    private Date createTime;
    /** 状态 */
    private Integer status;
}
