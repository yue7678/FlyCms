package com.flycms.modules.data.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * IP地址库数据传输对象 fly_ip_address
 * 
 * @author admin
 * @date 2020-12-10
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class IpAddressDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** IP段起始 */
    @Excel(name = "IP段起始")
    private String startIp;
    /** IP段结束 */
    @Excel(name = "IP段结束")
    private String endIp;
    /** 国家 */
    @Excel(name = "国家")
    private String country;
    /** 省份 */
    @Excel(name = "省份")
    private String province;
    /** 市级地区 */
    @Excel(name = "市级地区")
    private String city;
    /** 县 */
    @Excel(name = "县")
    private String county;
    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

}
