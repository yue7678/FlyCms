package com.flycms.modules.data.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 内容与标签关联数据传输对象 fly_label_merge
 * 
 * @author admin
 * @date 2020-11-18
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class LabelMergeDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 标签id */
    @Excel(name = "标签id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long labelId;
    /** 话术id */
    @Excel(name = "话术id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long infoId;
    /** 信息类别 */
    @Excel(name = "信息类别")
    private Integer infoType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
