package com.flycms.modules.data.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.data.mapper.LabelMapper;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.dto.LabelDTO;
import com.flycms.modules.data.service.ILabelService;

import java.util.ArrayList;
import java.util.List;

/**
 * 标签Service业务层处理
 * 
 * @author admin
 * @date 2020-11-18
 */
@Service
public class LabelServiceImpl implements ILabelService 
{
    @Autowired
    private LabelMapper labelMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增标签
     *
     * @param label 标签
     * @return 结果
     */
    @Override
    public int insertLabel(Label label)
    {
        label.setId(SnowFlakeUtils.nextId());
        label.setCreateTime(DateUtils.getNowDate());
        return labelMapper.insertLabel(label);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除标签
     *
     * @param ids 需要删除的标签ID
     * @return 结果
     */
    @Override
    public int deleteLabelByIds(Long[] ids)
    {
        return labelMapper.deleteLabelByIds(ids);
    }

    /**
     * 删除标签信息
     *
     * @param id 标签ID
     * @return 结果
     */
    @Override
    public int deleteLabelById(Long id)
    {
        return labelMapper.deleteLabelById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改标签
     *
     * @param label 标签
     * @return 结果
     */
    @Override
    public int updateLabel(Label label)
    {
        return labelMapper.updateLabel(label);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验标签名称是否唯一
     *
     * @param id ID
     * @param label_name 标签名称
     * @return 结果
     */
     @Override
     public String checkLabelLabelNameUnique(Long id,String label_name)
     {
         Label label = new Label();
         label.setId(id);
         label.setTitle(label_name);
         int count = labelMapper.checkLabelLabelNameUnique(label);
         if (count > 0){
             return UserConstants.NOT_UNIQUE;
         }
         return UserConstants.UNIQUE;
     }


    /**
     * 查询标签
     * 
     * @param id 标签ID
     * @return 标签
     */
    @Override
    public LabelDTO findLabelById(Long id)
    {
        Label  label=labelMapper.findLabelById(id);
        return BeanConvertor.convertBean(label,LabelDTO.class);
    }


    /**
     * 查询标签列表
     *
     * @param label 标签
     * @return 标签
     */
    @Override
    public Pager<LabelDTO> selectLabelPager(Label label, Integer page, Integer limit, String sort, String order)
    {
        Pager<LabelDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(label);

        List<Label> labelList=labelMapper.selectLabelPager(pager);
        List<LabelDTO> dtolsit = new ArrayList<LabelDTO>();
        labelList.forEach(entity -> {
            LabelDTO dto = new LabelDTO();
            dto.setId(entity.getId());
            dto.setTitle(entity.getTitle());
            dto.setContent(entity.getContent());
            dto.setCountTopic(entity.getCountTopic());
            dto.setCountGroup(entity.getCountGroup());
            dto.setCreateTime(entity.getCreateTime());
            dto.setStatus(entity.getStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(labelMapper.queryLabelTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的标签列表
     *
     * @param label 标签
     * @return 标签集合
     */
    @Override
    public List<LabelDTO> exportLabelList(Label label) {
        return BeanConvertor.copyList(labelMapper.exportLabelList(label),LabelDTO.class);
    }
}
