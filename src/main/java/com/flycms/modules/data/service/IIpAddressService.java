package com.flycms.modules.data.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.IpAddress;
import com.flycms.modules.data.domain.dto.IpAddressDTO;
import com.flycms.modules.data.domain.vo.IpAddressVO;

import java.util.List;

/**
 * IP地址库Service接口
 * 
 * @author admin
 * @date 2020-12-10
 */
public interface IIpAddressService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增IP地址库
     *
     * @param vo IP地址库
     * @return 结果
     */
    public int insertIpAddress(IpAddressVO vo);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除IP地址库信息
     *
     * @param id IP地址库ID
     * @return 结果
     */
    public int deleteIpAddressById(Long id);

    /**
     * 批量删除IP地址库
     *
     * @param ids 需要删除的IP地址库ID
     * @return 结果
     */
    public int deleteIpAddressByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改IP地址库
     *
     * @param vo IP地址库
     * @return 结果
     */
    public int updateIpAddress(IpAddressVO vo);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询IP地址库
     * 
     * @param id IP地址库ID
     * @return IP地址库
     */
    public IpAddressDTO findIpAddressById(Long id);

    /**
     * 查询当前大于等于起始段ip并且结束段ip小于当前最近一个ip起始段
     *
     * @param startIp  起始段ip
     * @param endIp   结束段ip
     *
     * @return
     */
    public List<IpAddress> findIpAddress(Long startIp,Long endIp);

    /**
     * 查询IP地址
     *
     * @param ip IP地址
     * @return IP地址详细信息
     */
    public IpAddressDTO findSearchIpAddress(String ip);

    /**
     * 查询IP地址库列表
     * 
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    public Pager<IpAddressDTO> selectIpAddressPager(IpAddress ipAddress, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    public List<IpAddressDTO> exportIpAddressList(IpAddress ipAddress);
}
