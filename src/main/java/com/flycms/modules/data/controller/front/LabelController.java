package com.flycms.modules.data.controller.front;

import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.modules.data.domain.dto.LabelDTO;
import com.flycms.modules.data.service.ILabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LabelController extends BaseController{
    @Autowired
    private ILabelService labelService;

    /**
     * 首页
     *
     * @return
     */
    @GetMapping(value = {"/label/" , "/label/index"})
    public String labelList(@RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap){
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("label/list_label");
    }

    /**
     * 获取解梦内容详细信息
     */
    @GetMapping(value = "/label/{id}")
    public String getInfo(@PathVariable(value = "id", required = false) String id, @RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap)
    {
        if (!StrUtils.checkLong(id)) {
            return forward("/error/404");
        }
        LabelDTO content= labelService.findLabelById(Long.valueOf(id));
        if(content == null){
            return forward("/error/404");
        }
        modelMap.addAttribute("p",p);
        modelMap.addAttribute("content",content);
        return theme.getPcTemplate("/label/detail");
    }
}
