$(document).ready(function(){
//关注小组
    $(document).on('click', '#follow-button', function (){
        var followId = $(this).attr("data-user-id");
        var text = $(this);
        $.ajax({
            url: '/user/follow',
            data: {"followId":followId},
            dataType: "json",
            type :  "post",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==0){
                    layer.msg(data.msg, {icon: 2, time: 2000});
                    return false;
                }else if(data.code==1){
                    layer.msg(data.msg, {icon: 2, time: 2000});
                    return false;
                }else if(data.code==2){
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==3){
                    text.removeClass('btn-success').addClass('alert-info');
                    if(text.text() == "关注"){
                        text.html("取消关注");
                    }
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==4){
                    text.removeClass('alert-info').addClass('btn-success');
                    if(text.text() == "取消关注"){
                        text.html("关注");
                    }
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 2, time: 2000});
                    return false;
                }
            }
        });
    });

});